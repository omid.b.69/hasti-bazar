import React from "react";
import { appWithTranslation, useTranslation } from 'next-i18next'
import { CssBaseline } from '@mui/material';
import PropTypes from 'prop-types';


import '../styles/main.css'
import MainContainer from "../components/rtl";
import Head from "next/head";
import Layout from "../components/layout";


const MyApp = (props:any) => {
    const { Component, pageProps } = props;
    const { i18n } = useTranslation('common');

    return (
        <>
            <Head>
                <title>{i18n.language === 'fa' ?  'هستی بازار' : 'hasti bazaar'}</title>
            </Head>
            <MainContainer>
                <Layout>
                    <CssBaseline />
                    <Component {...pageProps} />
                </Layout>
            </MainContainer>
        </>
    );
};

export default appWithTranslation(MyApp)

MyApp.propTypes = {
    Component: PropTypes.elementType.isRequired,
    emotionCache: PropTypes.object,
    pageProps: PropTypes.object.isRequired,
};
