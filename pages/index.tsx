import type { NextPage } from 'next'
import { useTranslation } from 'next-i18next'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import React from "react";
import {Box, Typography, Button } from '@mui/material'
import styles from './index.module.css'

const Home: NextPage = () => {
  const { t } = useTranslation('common')

  return (
      <>
          <Box mt={5} className={styles.box__nav}>
              <Typography variant={'h6'} >{t("home_title")}</Typography>
              <Box>
                  <Button>فارسی</Button>
                  <Button>English</Button>
              </Box>
          </Box>
      </>
  )
}
interface props {
    locale: string
}
export const getStaticProps = async ({locale}:props) => ({
  props: {
    ...await serverSideTranslations(locale, ['common']),
  },
})

export default Home
