
import React, { useState, createContext, useMemo, useEffect, ReactNode } from 'react';
import { ThemeProvider, createTheme } from '@mui/material/styles';
import { useTranslation } from 'react-i18next';
import rtlPlugin from 'stylis-plugin-rtl';
import { CacheProvider } from '@emotion/react';
import createCache from '@emotion/cache';
import {prefixer} from "stylis";
interface Props {
    children: any
}

const MainContainer = ({ children } : Props) => {
    const { i18n } = useTranslation('common')
    const [dir, setDir] = useState<"ltr" | "rtl">(i18n.language === 'fa' ? 'rtl' : 'ltr');
    useEffect(() => {
        document.dir = dir
    }, [dir])

    const theme = useMemo(() => {
        const font = '"iranyekan", "Arial", "sans-serif"';

        return createTheme({
            direction: dir,
            typography: {
                fontFamily: font,
                htmlFontSize: 16,
                h6: {
                    fontSize: '1.07143rem',
                }
            },
            palette: {
                mode: 'dark',
            },
        });
    }, [dir]);

    const cacheRtl = useMemo(() => {
        if (dir === 'rtl') {
            return createCache({
                key: 'muirtl',
                stylisPlugins: [prefixer, rtlPlugin],
                prepend: true
            });
        } else {
            return createCache({ key: 'css', prepend: true });
        }
    }, [dir]);

    return (
        <CacheProvider value={cacheRtl}>
            <ThemeProvider theme={theme}>
                {children}
            </ThemeProvider>
        </CacheProvider>
    )
}
export default MainContainer;