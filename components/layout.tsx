import styles from './layout.module.css'
import React from 'react';
import Container from '@mui/material/Container';
import {ReactNode} from 'react'

interface Props {
    children: ReactNode
}

function Layout({ children }: Props) {
    return (
        <>
            <Container maxWidth="md" className={styles.main}>
                {children}
            </Container>
        </>
    )
}

export default Layout;